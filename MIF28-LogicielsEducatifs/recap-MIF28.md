**Logiciels Educatifs [MIF28] (3 ECTS)** 

__Responsable __: Stéphanie JEAN-DAUBIAS

__Site de l’UE __: https://perso.liris.cnrs.fr/stephanie.jean-daubias/enseignement/LogEdu/

__Organisation __: distanciel ("pas de présentiel obligatoire avant Mars")
    - 15h CM, 15h TP
    - liens visio : https://univ-lyon1.webex.com/webappng/sites/univ-lyon1/dashboard/pmr/stephanie.jean-daubias

__Rocketchat __: 

__Evaluation __:  
    - 10% Cours et TP tests de logiciels
    - 55% Projet (fiche conception + rendu vidéo)
    - 35% Examen

__Dates importantes (rendus, examens ...) __: 
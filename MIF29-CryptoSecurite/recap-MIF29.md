**Cryptographie et sécurité [MIF29] (3 ECTS)** 

__Responsable __: GAVIN GERALD

__Support de cours __: https://cdn.discordapp.com/attachments/785809005364117544/808633864095924224/CoursCrypto2021_2.pdf

__Organisation __: 
    - 15h CM, 5h TD, 10h TP
    - liens visio : https://classe-info.univ-lyon1.fr/gav-qnk-sqg-r8l
    - Créneau : Mercredi 14h - 17h15 (Toutes les deux semaines càd quand les alternants sont là) 

__Rocketchat __: 

__Evaluation __:

__Dates importantes (rendus, examens ...) __:

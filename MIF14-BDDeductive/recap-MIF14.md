**Bases de Données Déductives [MIF14] (3 ECTS)** 

__Responsable __: Angela BONIFATI

__Site de l’UE __: https://drive.google.com/drive/folders/1yvwRSeEn4e9kEy3u-StX4bQOc0wzdiUv

__Organisation __: 
    - 10h CM, 10h TD, 10h TP
    - lien visio (CM & TD groupe C) :  https://univ-lyon1-fr.zoom.us/j/89172714121?pwd=QTNCa0dMK0trYm45S3F0N0JtbmlxUT09
    - lien visio (TD groupe A) : https://classe-info.univ-lyon1.fr/pin-qxk-mh3-7xy
    - lien visio (TD groupe B) : https://classe-info.univ-lyon1.fr/hac-u0u-wg0-yfm


__Rocketchat (CM & TD) __: 
    - https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FQaqavh
    - https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2F37CMv5

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
    - CM1 12/04 9h00-10h30
    - TD1 12/04 10h45-12h15

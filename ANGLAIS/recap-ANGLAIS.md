**🇬🇧 Anglais GLOBAL 🇬🇧 (3 ECTS)**

__Responsables __: Nathalie DOURLOT + selon les groupes
    - A : Mireille Huber
    - B : Sylvie Vert
    - C :
    - D : Olivier Bouguin-Vasiljevic
    - E : Olivier Bouguin-Vasiljevic
    - F :
    - G :

__Site de l’UE __: (SCEL) http://scel.univ-lyon1.fr

__Organisation __: distanciel
    - dépend des profs de chaque groupe, mais 10 créneaux de 3h

__Organisation A __:
    - Le lundi de 14h-17h15
    - Discord : https://discord.gg/JwCWtVe

__Organisation B __:
    - Le lundi de 14h-18h
    - Site : http://sylvievert.com/
    - Supports de cours : https://drive.google.com/drive/folders/1bjoxtkP6-9GNVkHocVRW4oBJ5Pvbmz6y?usp=sharing
    - Webex : https://univ-lyon1.webex.com/meet/sylvie.vert

__Organisation D __:
    - Le mercredi de 9h45 à 13h
    - Site :
    - Supports de cours :
    - Webex : https://univ-lyon1.webex.com/meet/olivier.bouguin-vasiljevic
    - ! Premier cours le 17/02

__Organisation E __:
    - Le mercredi de 14h à 17h
    - Site :
    - Supports de cours :
    - Webex : https://univ-lyon1.webex.com/meet/olivier.bouguin-vasiljevic
    - Premier cours le 10/02

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:

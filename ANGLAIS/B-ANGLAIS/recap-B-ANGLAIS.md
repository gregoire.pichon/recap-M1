**🇬🇧 Anglais GROUPE B 🇬🇧 (3 ECTS)**

__Responsable __: Sylvie Vert

__Site de l’UE __: http://sylvievert.com/

__Organisation __:
    - Le lundi de 14h-18h
    - Site : http://sylvievert.com/
    - Supports de cours : https://drive.google.com/drive/folders/1bjoxtkP6-9GNVkHocVRW4oBJ5Pvbmz6y?usp=sharing
    - Webex : https://univ-lyon1.webex.com/meet/sylvie.vert

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:

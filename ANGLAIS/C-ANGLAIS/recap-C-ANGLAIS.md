**🇬🇧 Anglais GROUPE C 🇬🇧 (3 ECTS)**

__Responsable __: Patricia Miramand-Gravrand

__Site de l’UE __: **BFM INFO Master English GpC Miramand (Master Info Miramand 2021)** https://clarolineconnect.univ-lyon1.fr

__Organisation __: 
- Mardi Matin de 8h à 11h15
- liens visio :https://univ-lyon1.webex.com/meet/patricia.miramand-gravrand

__Evaluation __:  Voir !deadlines
- TOEIC - 10% (Listening + Reading)
- Cover Letter - 10%
- Listening Comprehension - 20%
- Job Interview + IT Innovation - 30%
- Text Reading - 15%
- Classwork - 15%

__Dates importantes (rendus, examens ...) __: 
- 13/04 : IT INNOVATION PRESENTATIONS
- 02/06 : TOEIC Session
- 08/06 : JOB INTERVIEWS

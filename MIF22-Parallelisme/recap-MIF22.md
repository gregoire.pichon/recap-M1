**Parallélisme [MIF22] (3 ECTS)**

__Responsable __: Laurent LEFEVRE

__Site de l’UE __: https://perso.ens-lyon.fr/laurent.lefevre/M1_parallelisme/

__Organisation __:
    - 12.5h CM, 7.5h TD, 10h TP
    - liens visio : Change à chaque CM

__Rocketchat __:

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
    - CM 18/02 de 13h00-16h00
    - CM 19/02 de 13h00-16h00
    - CM 18/03 de 13h00-16h00
    - CM 08/04 de 13h00-15h00
    - TP1 15/04 de 13h00-16h00 (activer popup sinon demandera un mdp) : https://ent-services.ens-lyon.fr/entVisio/quickjoin.php?hash=5afa5c27a5e6cb416c4a77b352a6c7f7aa82cd6e9651509791ded51aee83bdbc&meetingID=6530
    - 10/06 : peut-être un examen

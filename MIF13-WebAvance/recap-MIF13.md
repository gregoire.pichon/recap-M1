**Programmation Web avancée et mobile [MIF13] (3 ECTS)** 

__Responsable __: Lionel MEDINI

__Site de l’UE __:  https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF13/

__Liens Utiles __:   https://forge.univ-lyon1.fr/m1if13/m1if13-2021/

__Organisation __: Distanciel
    - 15h CM, 15h TP
    - Mardi après-midi en semaines alternants : 14h00-15h30 : CM, 15h45-19h00 : TP
    - liens visio : https://classe-info.univ-lyon1.fr/med-6g9-cpe-tfk

__Rocketchat __: 
    - https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2F7XCvoT 
    - https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2F5FxkDS

__Evaluation __:  

__Dates importantes (rendus, examens ...) __: https://calendar.google.com/calendar/u/0?cid=ZWZoNmg0ZWplaXBncjdtaTgzNjEwaXRmNm9AZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ
    - 09/02 14h : CM + TP
    - 16/02 14h : CM + TP
    - 09/03 14h : CM + TP
    - 16/03 14h : CM + TP
    - 06/04 14h : CM + TP
    - 13/04 14h : CM + TP
    - 04/05 14h : CM + TP
    - 11/05 14h : CM + TP
    - 01/06 14h : CM + TP
    - 08/06 14h : CM + TP
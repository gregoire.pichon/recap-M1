# Récap M1 2020/2021
___

## News

Rien pour le moment

___
# Le dépot

## De quoi 

Ce dépot git répertorie l'ensemble des liens, dates, et autres infos importantes liées aux différentes UE du S2 de M1 2020/2021

## Pourquoi

Le but de ce dépot est de centraliser l'information, en effet entre l'EDT qui n'est pas à jour, les mails et les différents sites des responsables d'UE il est facile de perdre du temps ou de se tromper de liens.

En centralisant l'information et rendant sa modification accesible à tous nous avons donc une base constament mise à jour par les différents acteurs facilement accessible.


## Comment participer

Pour participer il vous suffit de posséder un compte sur la forge de l'université Lyon 1 et nous fonctionnons avec le principe de fork / merge request.


___
# Tuto fork / merge request 
Faites un fork du git :

![git_fork](ressources/img/howto_fork.png)

![git_fork_done](ressources/img/howto_fork_done.png)

Ouvrez le fichier du récap que vous voulez modifier dans votre éditeur de texte préféré (ici gitlab IDE) :

![git_ide](ressources/img/howto_gitIde.png)

![git_ide_open](ressources/img/howto_gitIde_open.png)

:warning: Vous devez respecter la syntaxe, malgré l'extension `.md` la syntaxe est différente du markdown, cela pour faciliter l'intégration externe :warning:

![git_ide_done](ressources/img/howto_gitIde_done.png)

Faites un commit sur master de vos modifications :

![git_commit](ressources/img/howto_commit.png)

Ensuite quand vous êtes satisfait de vos modifications vous pouvez lancer le processus de merge request :

![git_mr](ressources/img/howto_mr_start.png)

Remplissez la merge request avec titre, description et labels et soumettez-la.

![git_mr_filled](ressources/img/howto_mr_filled.png)

Bravo ! 
Vous n'avez plus qu'à attendre que la merge request soit validée et tout est bon, si il y a un problème avec les modifications vous en serez notifié dans gitlab nous pourrons discuter dans l'interface gitlab de pourquoi et comment modifier votre merge request pour qu'elle soit acceptée.

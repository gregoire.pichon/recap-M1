**Synthèse d'image [MIF27] (3 ECTS)** 

__Responsable __: Vincent NIVOLIERS

__Site de l’UE __: https://perso.liris.cnrs.fr/vincent.nivoliers/mif27/

__Organisation __: distanciel
    - 15h CM, 15h TP
    - liens visio : https://classe-info.univ-lyon1.fr/niv-cuy-h7p-775

__Rocketchat __: https://chat-info.univ-lyon1.fr/group/mif27

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
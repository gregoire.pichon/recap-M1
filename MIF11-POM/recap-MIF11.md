**Projet pour l'orientation en Master [MIF11] (6 ECTS)**

__Responsables __: Sylvain BRANDEL + encadrant.es par sujet

__Sites de l’UE __: https://perso.liris.cnrs.fr/sylvain.brandel/wiki/doku.php?id=ens:pom
    - liste des sujets : https://tomuss.univ-lyon1.fr/S/2020/Automne/public_login/2020/Public/M1POM

__Organisation __: Distanciel

__Evaluation __: 

__Dates importantes (rendus, examens ...) __:
    - 30/12/2020 23h59 RENDU : cahier des charges (dépôt Tomuss)
    - 28/05/2021 23h59 RENDU : rapport final [1/binôme, pdf : cdc_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf, 4 pages max] (dépôt Tomuss)
    - 31/05/2021 23h59 RENDU : poster [1/binôme, taille A1, pdf : poster_IDgrp_nom1_numetudiant1_nom2_numetudiant2.pdf] (dépôt Tomuss)
    - 03/06/2021 14h-18h ORAL : présentation autour du poster, ordinateur possible pour démos
    - 04/06/2021 23h59 RENDU : vidéo de vulgarisation [1/binôme, 3min max, format lisible au moins par VLC : video_IDgrp_nom1_numetudiant1_nom2_numetudiant] (dépôt Tomuss)
    - 14/06/2021 NOTES (pour les enseignants)

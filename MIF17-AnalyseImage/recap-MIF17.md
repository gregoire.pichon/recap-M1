**Analyse d'Image [MIF17] (3 ECTS)**

__Responsable __: Saida BOUAKAZ BRONDEL

__Site de l’UE __: http://xxx
    - support cours partie 1 : https://drive.google.com/file/d/1iurBuAFTx3CvkdWyI0A1R36X1VkkbsHm/view

__Organisation __: distanciel, présentiel sur demande pour les TP
    - 15h CM, 7h TD, 8h TP
    - liens visio : envoyé par mail (Webex)
    - CM (28/04) : https://univ-lyon1.webex.com/univ-lyon1/e.php?MTID=mff3f264b4588a434f8c30dbab297d968

__Rocketchat __: https://chat-info.univ-lyon1.fr/group/mif-17-analysedimage

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
    - TP1 décalé d'une semaine parce que le cours n'a pas assez avancé

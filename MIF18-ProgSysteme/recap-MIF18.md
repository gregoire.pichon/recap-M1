**Programmation système et temps réel [MIF18] (3 ECTS)** 

__Responsable __: Grégoire PICHON

__Site de l’UE __: http://laure.gonnord.pages.univ-lyon1.fr/advanced-systems/

__Organisation __: 
    - 9h CM, 21h TP
    - liens visio : https://classe-info.univ-lyon1.fr/pic-qvk-qsf-tbo

__Rocketchat __: https://chat-info.univ-lyon1.fr/group/mif18-2021

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
    - 14/05 avec Jean-Patrick Gelas à 9h ici : https://classe-info.univ-lyon1.fr/gel-tel-zll-ko9

**Animation en synthèse d'image [MIF37] (3 ECTS)** 

__Responsable __: Alexandre MEYER

__Site de l’UE __: http://xxx 
    - https://perso.liris.cnrs.fr/ameyer/public_html/www/doku.php?id=charanim_m1
    - https://perso.liris.cnrs.fr/florence.zara/Web/M1Animation.html

__Organisation __: 
    - 12h CM, 18h TP
    - liens visio : 

__Rocketchat __: 

__Evaluation __:  

__Dates importantes (rendus, examens ...) __:
    - CCF: Mardi 22 Juin de 8h à 9h30
    - Rendu TP: Mardi 22 Juin à 18h (code + vidéo de démonstration de chaque partie)

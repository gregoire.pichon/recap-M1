**Réseaux par la pratique [MIF15] (3 ECTS)** 

__Responsable __: Elise JEANNEAU

__Site de l’UE __: http://xxx
    - précédemment : perso.univ-lyon1.fr/fabien.rico/site/m1if15:2018:start

__Organisation __:
    - 2.5h CM, 27.5h TP
    - liens visio : 17/02 : https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=mba9cb153f7671a28b72df07c09b1f69b

__Rocketchat __: https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2Fig8njf

__Evaluation __: TP noté + Examen

__Dates importantes (rendus, examens ...) __:
    - CM 10/02 8h00 Intro en présentiel TD3 
    - TP 10/02 9h45 Intro en présentiel TPR2 / TPR3
    - TP 17/02 9h00 spanning-tree vlan en distanciel
    - CM 10/03 8h00 IPV6 
    - TP 10/03 9h15 découpage IPV4
    - TP 17/03 9h00 IPV6
    - TP 07/04 9h00 TP noté
    - TP 14/04 9h00 routage statique
    - TP 05/05 9h00 routage dynamique
    - TP 12/05 9h00 dhcp + nate
    - TP 02/06 9h00 ACL
    - TP 09/06 9h00 Examen


**Algorithmique Distribuée [MIF12] (3 ECTS)**

__Responsable __: Isabelle GUERIN LASSOUS

__Site de l’UE __: http://perso.ens-lyon.fr/isabelle.guerin-lassous/index-M1if12.htm
    - Année dernière : http://perso.ens-lyon.fr/isabelle.guerin-lassous/index-AD.htm
    - Accès PDF de l'an dernier et de cette année : Login: ||MIF12|| Mot de passe: ||12M1AD||

__Organisation __:
    - 12h CM, 9h TD, 9h TP
    - lien visio (CM) : https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=mc6b1d84a3fce7e0bbd01abe20619c8eb
    - lien visio (TD groupe C E. JEANNEAU) : https://classe-info.univ-lyon1.fr/jea-zay-wnm-wfv

__Rocketchat __:
    - groupe C (Elise JEANNEAU) notamment pour l'appel : https://chat-info.univ-lyon1.fr/group/mif12-groupe-c

__Evaluation __: (Peut être modifier à l'avenir)
    - 2 * QCM 
    - Note de TP 
    - 1 * ECA

__Dates importantes (rendus, examens ...) __:
    - QCM le 31 mai lors du TD3 qui aura lieu en présentiel
    - Note de TP - 29 Avril (Voir deadlines)
    - ECA le 10 juin en présentiel
    - QCM le 10 juin en présentiel

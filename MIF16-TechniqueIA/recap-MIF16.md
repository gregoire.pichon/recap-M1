**Technique de l'Intelligence Artificielle [MIF16] (3 ECTS)**

__Responsables __: Simon FOREST, Mathieu LEFORT, Samir AKNINE

__Sites de l’UE __: https://perso.liris.cnrs.fr/sforest/tia/

__Organisation __: distanciel pour l'instant, TP en présentiel sur demande (groupe P = présentiel)
    - 18h CM, 12h TP
    - découpé en 3 parties indépendantes (profs et évaluations séparées ...)

__Orga PARTIE 1 __: Prise de décision d'un agent cognitif (Simon FOREST)
    - 28/01 au 25/03
    - 3 CM, 6h TP
    - site : https://perso.liris.cnrs.fr/sforest/tia/
    - liens visio (Partie 1): https://classe-info.univ-lyon1.fr/for-daj-sfa-8xj

__Orga PARTIE 2 __: Réseaux de neurones (Mathieu LEFORT)
    - 25/03 au 29/04
    - 4.5 CM, 6h TP
    - site : https://clarolineconnect.univ-lyon1.fr/workspaces/73104/open/tool/resource_manager
    - liens visio (Partie 2): https://univ-lyon1.webex.com/meet/mathieu.lefort

__Orga PARTIE 3 __: Systèmes multi-agents (Samir AKNINE)
    - 20/05 au 24/06
    - 3 CM, 7.5h TP
    - site :
    - liens visio :

__Rocketchat __:
    - Partie 1 :
        - invitation (la plus récente) : https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FoNqwa2
        - chat : https://chat-info.univ-lyon1.fr/group/tia

__Evaluation __:
    - Partie 1 : rendu TP2 (avant le 26/03)

__Dates importantes (rendus, examens ...) __:
    - Jeudi 04/03 de 8h à 11h15 : TP groupe 2
    - Jeudi 04/03 de 11h30 à 13h + vendredi 05/03 de 8h à 9h30 : TP groupe 1
